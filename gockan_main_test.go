package gockan_test

import (
  "os";
  "fmt";
  "context";
  "github.com/jenazads/gockan";
)

func packageNames() ([]string, error) {
  var names []string

  //cli, err := gockan.NewGockan("http://data.cityofdenton.com/")
  //cli, err := gockan.NewGockanHttpOptions("http://data.cityofdenton.com/", nil)
  //cli, err := gockan.NewGockanAgentOptions("http://data.cityofdenton.com/", "gockan/","1", nil)
  cli, err := gockan.NewGockanApiOptions("http://data.cityofdenton.com/", "api/", "1", "gockan/","2", nil)
  if err != nil {
    return names, fmt.Errorf("could not create new client: %v", err)
  }
  ctx := context.Background()

  packages, _, err := cli.GetPackages().List(ctx, &gockan.QueryOptions{Limit: 10})
  if err != nil {
    fmt.Fprintf(os.Stderr, "could not fetch packages: %v", err)
    os.Exit(2)
  }

  for _, pkg := range packages {
    fmt.Println(pkg)
  }

  var m []gockan.GockanDataStoreMetadata

  for l, o := 100, 0; ; o += l {
    metadata, _, err := cli.GetDataStore().TableMetadata(ctx, &gockan.QueryOptions{Limit: l, Offset: o})
    if err != nil {
      fmt.Fprintf(os.Stderr, "could not fetch table metadata: %v", err)
      os.Exit(2)
    }
    m = append(m, metadata...)

    if len(metadata) < l {
      break
    }
  }

  fmt.Println(len(m))

  req, err := cli.GockanRequest("GET", "action/package_list?limit=10", nil)
  if err != nil {
    return names, fmt.Errorf("coud not create request: %v", err)
  }

  _, err = cli.Do(ctx, req, &names)
  if err != nil {
    return names, fmt.Errorf("could not perform request: %v", err)
  }
  return names, nil
}

func Example_getPackages(){
  names, err := packageNames()
  if err != nil {
    fmt.Fprintf(os.Stderr, "could not fetch packages: %v", err)
    os.Exit(2)
  }

  for _, name := range names {
    fmt.Println(name)
  }
}
