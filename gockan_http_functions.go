package gockan

import (
  "io";
  "fmt";
  "bytes";
  "context";
  "net/url";
  "net/http";
  "encoding/json";
)

// Make a new Request and create an API url. you can add a relative url in subURL
// that subURL is resolved with baseURL of the client.

// Relative URLs should always be specified without a preceding slash. If
// specified, the value pointed to by body is JSON encoded and included as the
// request body.
func (o *Gockan) GockanRequest(method string, urlStr string, body interface{}) (*http.Request, error) {
  rel, err := url.Parse(urlStr)
  if err != nil {
    return nil, err
  }

  u := o.GetBaseURL().ResolveReference(rel)

  var buf io.ReadWriter
  if body != nil {
    buf = new(bytes.Buffer)
    err := json.NewEncoder(buf).Encode(body)
    if err != nil {
      return nil, err
    }
  }
  req, err := http.NewRequest(method, u.String(), buf)
  if err != nil {
    return nil, err
  }
  
  // get query values
  for key, value := range o.GetRequestHeaders() {
    req.Header.Set(key, value)
  }
  reqHeader, _ := json.Marshal(req.Header)
  fmt.Println("Request: ", string(reqHeader))
  return req, nil
}

// Sends an API request and returns the API response (in JSON Format).
// ctx must be non-nil. If it is canceled or times out, -> ctx.Err()
func (o *Gockan) Do(ctx context.Context, req *http.Request, jsonresp interface{}) (*http.Response, error) {
  req = req.WithContext(ctx)
  resp, err := o.GetCkanClient().Do(req)
  // is err -> ctx has been canceled
  if err != nil {
    select {
      case <-ctx.Done():
        return nil, ctx.Err()
      default:
    }
    return nil, err
  }
  defer resp.Body.Close()

  // evaluate status, if ir different from 200, resp will not decode
  if statusResp := resp.StatusCode; statusResp < 200 || statusResp > 299 {
    err := &CatchError{statusResp}
    return resp, err.EvalError()
  }

  // decode to JSON the API response
  response := new(jsonResponse)
  if err := json.NewDecoder(resp.Body).Decode(response); err != nil {
    return resp, err
  }
  if !response.IsSuccess {
    return resp, response.Error
  }
  if jsonresp != nil {
    err = json.Unmarshal(response.Result, jsonresp)
  }
  respHeader, _ := json.Marshal(resp.Header)
  fmt.Println("Response: ", string(respHeader))
  return resp, err
}
