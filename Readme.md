# golang + CKAN = GocKan (Gokan)

GocKan (Golang for CKAN) is an golang API implementation for CKAN DataSet Platform.  
You can see an extended doc in [godocs](https://godoc.org/github.com/Jenazads/gockan).

## CKAN 

CKAN is a DataSet open source platform that you can upload your information and manipulates as JSON or CSV format to consume data.  
See more information [here](https://github.com/ckan/ckan).

## GocKan

* You can donwload trying this:

        go get github.com/jenazads/gockan

* Then, create a simple object client:
